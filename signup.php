<!DOCTYPE html>
<html lang="en">

<head>
    <title>Farm 2 Kitchen</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/bootstrap.css">
   
    <link rel="stylesheet" href="dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/animate.min.css">
    <link rel="stylesheet" href="dist/css/bootstrap-dropdownhover.min.css">
    <link rel="stylesheet" href="dist/css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="dist/js/bootstrap-dropdownhover.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <script type="text/javascript">
			//function to check validation (Required field)
			function checkReqFields(){
				var returnValue;
				var name=document.getElementById("txtName").value;
            var last=document.getElementById("txtLast").value;
            var email=document.getElementById("txtEmail").value;
            var pass=document.getElementById("txtPass").value;
            var phone=document.getElementById("txtPhone").value;
            var city=document.getElementById("txtCity").value;
		      returnValue=true;
				if(name.trim()==""){
					document.getElementById("reqTxtName").innerHTML="* Name is required.";
					returnValue=false;
				}
            if(last.trim()==""){
					document.getElementById("reqTxtLast").innerHTML="* Lastname is required.";
					returnValue=false;
				}
            if(email.trim()==""){
					document.getElementById("reqTxtEmail").innerHTML="* Email is required.";
					returnValue=false;
				}
            else{
                if(!(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/).test(email)){
                    document.getElementById("reqTxtEmail").innerHTML="Email is Invalid.";
                    returnValue=false;
                }


            }
            if(pass.trim()==""){
					document.getElementById("reqTxtPass").innerHTML="* Password is required.";
					returnValue=false;
				}
            else{
               if (!(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,}).{6,}$/).test(pass)){
               document.getElementById("reqTxtPass").innerHTML="Password is Invalid.";
               returnValue=false;
               }
            }
            if(phone.trim()==""){
					document.getElementById("reqTxtPhone").innerHTML="* Phone no is required.";
					returnValue=false;
				}
            if(city.trim()==""){
					document.getElementById("reqTxtCity").innerHTML="* City is required.";
					returnValue=false;
				}
            return returnValue;
           
			}
		</script>

    
</head>

<body style="overflow-x: hidden;">
        
    <nav class="navbar-fixed-top navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="font-family: 'Pacifico', cursive;">Farm2Kitchen</a>
            </div>
            <div>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class="dropdown">
                            <button class="btn1 dropdown-toggle" data-hover="dropdown">
                                HOME <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Homepage one</a></li>
                                <li><a href="#">Homepage two</a></li>
                                <li><a href="#">Homepage three</a></li>
                                <li class="dropdown">
                                    <a href="#">Header style</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Header one</a></li>
                                        <li><a href="#">Header two</a></li>
                                        <li><a href="#">Header three</a></li>
                                </li>
                            </ul>
                    </li>
                      </ul>
            </div>
            </li>
            <li><a href="#about"><strong>ABOUT US</strong></a></li>
            <li>
                <div class="dropdown">
                    <button class="btn1 dropdown-toggle" data-hover="dropdown">
                        GALLERY <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Gallery one</a></li>
                        <li><a href="#">Gallery two</a></li>
                        <li><a href="#">Gallery three</a></li>
                        <li class="dropdown">
                            <a href="#">Header style</a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Header one</a></li>
                                <li><a href="#">Header two</a></li>
                                <li><a href="#">Header three</a></li>
                        </li>
                    </ul>
                </ul>
            </div>
            </li>

        </li>
        <li> <div class="dropdown">
            <button class="btn1 dropdown-toggle" data-hover="dropdown">
                BLOG <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#">Our Blog</a></li>
                <li><a href="#">Single Blog</a></li>
    </ul>
    </div></li>
        <li><div class="dropdown">
            <button class="btn1 dropdown-toggle" data-hover="dropdown">
             SHOP <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#">Our Shop</a></li>
                <li><a href="#">Shop single</a></li>
                <li><a href="#">Shopping card</a></li>
                <li><a href="#">Checkout</a></li>
                
        </ul>
    </div></li>
        <li><a href="#contact"><strong>CONTACT US</strong></a></li>
        </ul>
        </div>
        </div>
    </nav><!-- /.navbar -->
<br><br><br>
    <div class="container-fluid px-0">
        <div class="row mx-0">
            <div class="col-12 px-0">
            <img src="img/goat1.jpeg" width=100% >
            </div>
        </div>
    </div>
        <br>

<div class="container">
<h3 style="font-family: 'Pacifico', cursive;">Create An Account</h3><br>
<form name="form1" method="post" action="signupuser.php" onsubmit="return checkReqFields()">
<div class="row">
<div class="col-xs-2">
   <input type="text" class="form-control1" id="txtName" name="firstname" placeholder="Firstname"><br>
   <span id="reqTxtName" style="color: #ff0000;"></span>
   <input type="text" class="form-control1" id="txtEmail" name="email" placeholder="Email"><br>
   <span id="reqTxtEmail" style="color: #ff0000;"></span>
   <input type="text" class="form-control1" id="txtPhone"  name="phone" placeholder="Phone"><br>
   <span id="reqTxtPhone" style="color: #ff0000;"></span>
        </div>
        <div class="form-group col-xs-2">
        <input type="text" class="form-control1" id="txtLast" name="lastname" placeholder="Lastname"><br>
   <span id="reqTxtLast" style="color: #ff0000;"></span>
   <input type="password" class="form-control1" id="txtPass" name="pass" placeholder="Password"><br>
   <span id="reqTxtPass" style="color: #ff0000;"></span>
       
     <input type="text" class="form-control1" id="txtCity" name="city" placeholder="City"><br>
    <span id="reqTxtCity" style="color: #ff0000;"></span>
   
        </div>
        </div>
        <div style="margin-left:120px">
        <span>
          <?php 
          if(isset($_GET['msgg']))
           echo $_GET['msgg'];
           ?>
         </span>
        </div>
     <button style="margin-left:150px"  name="submit" type="submit" id="submit" type="button" class="btn btn-default">SignUp</button>
        
        </form>
        </div>
        
        <br><br>
        <div class="panel panel-default">
  
  <div class="panel-body">
  <div class="row">
      <div class="col-sm-3">
      <h4 style="font-family: 'Pacifico', cursive;">Farm2Kitchen</h4><br>
      

<p>Farm2Kitchen is a company that sought out to deliver what the people wanted.
We heard the need for quality meat at a cheaper price - closer to your home.
</p>

<p>Our goat and chicken products were all fresh from the farm, 
organic, grass fed and free range. We believe pasture raised is absolutely, without a doubt, 
better for the animals — which is better for you!</p>

          </div> 
          <div class="col-sm-3">
              <h5><strong>KEEP IN TOUCH</strong></h5><br>
          <p>Phone #: 469-602-1319</p>

<p>Store Address: 700 W. Sping Creek Pkwy, Plano, TX, 75023</p>
          </div>
          <div class="col-sm-3">
              <h5><strong>INSTAGRAM FEED</strong></h5><br>
              <img src="img/ins1.jpg">
              <img src="img/ins2.jpg"><br>
              <img src="img/ins2.jpg">
              <img src="img/ins1.jpg">
          </div>
          <div class="col-sm-3">
              <h5><strong>FEATURED LINKS</strong></h5><br>
              <div class="row">
                  <div class="col-sm-5">
              
              <li>
                  <a href="#">About</a>
              </li>
              <li><a href="#">Testimonial</a></li>
              <li><a href="#">Team Members</a></li>
          
          </div>
          <div class="col-sm-5">
              
              <li><a href="#">What We Do</a></li>
              <li><a href="#">New Products</a></li>
              <li><a href="#">Our career</a></li>
          
          </div>
          </div><br>
          <h5><strong>FOLLOW US ON:</strong></h5>
          <div class="links">
                          <a href="#"><span class="fa fa-facebook-official"></span></a>
                          <a href="#"><span class="fa fa-twitter"></span></a>
                          <a href="#"><span class="fa fa-google-plus"></span></a>
                          <a href="#"><span class="fa fa-instagram"></span></a>
                          <a href="#"><span class="fa fa-vimeo-square"></span></a>
                          <a href="#"><span class="fa fa-youtube-play"></span></a>
                          <a href="#"><span class="fa fa-dropbox"></span></a>
                      </div>
          </div>
          </div>   
          </div>
<div class="panel-footer">Copyrights © Farm2Kitchen 2019. All rights reserved.</div>
          </div>
                

            </html>